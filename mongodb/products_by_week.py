from mongoengine import connect, Document, StringField, DateTimeField, IntField, FloatField, ValidationError, ObjectIdField
import os
from pymongo import UpdateOne
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from mongodb.config import config
import hashlib

user = config[os.environ.get('python_env')]['mongo_server_user']
pw = config[os.environ.get('python_env')]['mongo_server_pw']
cluster_url = config[os.environ.get('python_env')]['mongo_cluster_url']
db = config[os.environ.get('python_env')]['mongo_env']
conn_string = 'mongodb+srv://%s:%s@%s/%s?retryWrites=true&w=majority' % (user, pw, cluster_url, db)
connect('prod'
    , host=conn_string
)
from datetime import datetime
import pandas as pd
import logging
import os
from multiprocessing import Queue
from multiprocessing.pool import ThreadPool
logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)


class zoetis_products_by_week(Document):
    narc_id = IntField(required=True)
    narc_postal_code = StringField()
    narc_state = StringField()
    product_line = StringField(required=True, max_length=100)
    therapeutic_group = StringField()
    product_species_group_description = StringField()
    us_fiscal_year = IntField(required=True)
    us_fiscal_week_number = IntField(required=True)
    us_fiscal_year_month_number = IntField(required=True)
    net_amount = FloatField()
    net_quantity = FloatField()
    create_date = DateTimeField(required=True, default=datetime.now())
    update_date = DateTimeField(required=True, default=datetime.now())
    upsert_key = StringField(required=True, max_length=100)
    meta = {'collection': 'zoetis_products_by_week'
            , 'indexes': [
                            {
                                'name': 'upsert_key'
                                , 'fields': ['upsert_key']
                            }
                            ]
            }

    def __init__(self, *args, **kwargs):
        try:
            __slots__ = 'narc_id', 'narc_postal_code', 'narc_state', 'product_line', 'therapeutic_group', 'product_species_group_description'\
                , 'us_fiscal_year', 'us_fiscal_week_number', 'net_amount', 'net_quantity', 'create_date', 'update_date', 'upsert_key'
            super().__init__(*args, **kwargs)
            self.narc_id = kwargs['narc_id'] if 'narc_id' in kwargs else None
            self.narc_postal_code = self.handle_null_to_str(kwargs['narc_postal_code']) if 'narc_postal_code' in kwargs else None
            self.narc_state = self.handle_null_to_str(kwargs['narc_state']) if 'narc_state' in kwargs else None
            self.product_line = self.handle_null_to_str(kwargs['product_line']) if 'product_line' in kwargs else None
            self.therapeutic_group = self.handle_null_to_str(kwargs['therapeutic_group']) if 'therapeutic_group' in kwargs else None
            self.product_species_group_description = self.handle_null_to_str(kwargs['product_species_group_description']) if 'product_species_group_description' in kwargs else None
            self.us_fiscal_year = kwargs['us_fiscal_year'] if 'us_fiscal_year' in kwargs else None
            self.us_fiscal_week_number = kwargs['us_fiscal_week_number'] if 'us_fiscal_week_number' in kwargs else None
            self.us_fiscal_year_month_number = kwargs['us_fiscal_year_month_number'] if 'us_fiscal_year_month_number' in kwargs else None
            self.net_amount = kwargs['net_amount'] if 'net_amount' in kwargs else None
            self.net_quantity = kwargs['net_quantity'] if 'net_quantity' in kwargs else None
            # self.create_date = datetime.now()
            self.update_date = datetime.now()
            self.upsert_key = kwargs['id'] if 'id' in kwargs else hashlib.sha1(('{}{}{}{}{}'.format(self.narc_id, self.product_line, self.us_fiscal_year, self.us_fiscal_week_number, self.us_fiscal_year_month_number)).encode('utf8')).hexdigest()

        except Exception as e:
            raise e

    def handle_null_to_str(self, val):
        if pd.isnull(val):
            return None
        return val


def load_worker(load_queue):
    try:
        # connect('prod'
        #     , host=conn_string
        # )
        chunk = load_queue.get()
        while True:
            logger.debug('load_queue.qsize %i' % load_queue.qsize())
            try:
                if type(chunk) == pd.Series:
                    try:
                        bulk_operations = []
                        try:
                            for entity in chunk:
                                entity.validate()
                                bulk_operations.append(
                                    UpdateOne({'upsert_key': entity.upsert_key}
                                              , {'$set': entity.to_mongo().to_dict()}, upsert=True))

                            if bulk_operations:
                                collection = zoetis_products_by_week._get_collection() \
                                    .bulk_write(bulk_operations, ordered=False)
                        except ValidationError as ve:
                            logger.debug(ve)
                        except Exception as e:
                            logger.error(e)
                    except Exception as e:
                        logger.error(e)
                    del(chunk)
                    chunk = load_queue.get()
                elif chunk == 'poison':
                    break
                else:
                    raise Exception('Load worker does not recognize chunk %s' %(chunk))
            except Exception as e:
                logger.error(e)
    except Exception as e:
        logger.error(e)

def prep_chunk(temp_df, load_queue):
    chunk = temp_df.apply(map_to_table, axis=1)
    load_queue.put(chunk)
    logger.debug('load_queue.qsize %i' % load_queue.qsize())


def load_from_csv(fp, load_queue):
    df = pd.read_csv(fp)
    df = validate_df(df=df)
    try:
        prep_pool_size = 5
        prep_worker_pool = ThreadPool(prep_pool_size)
        offset = 0
        limit = 1000
        while len(df) > offset:
            temp_df = df[offset:offset + limit]
            prep_worker_pool.apply_async(prep_chunk, args=(temp_df, load_queue))
            offset += limit
        prep_worker_pool.close()
        prep_worker_pool.join()
    except Exception as e:
        logger.error(e)

def map_to_table(row):
    # row = validate_record(record=row)
    if row is None:
        return
    record = zoetis_products_by_week(narc_id=int(row['NARC ID'])
                                     , narc_postal_code=row['NARC Postal Code']
                                     , narc_state=row['NARC State']
                                     , product_line=row['Product Line']
                                     , therapeutic_group=row['Therapeutic Group']
                                     , product_species_group_description=row['Product Species Group Description']
                                     , us_fiscal_year=int(row['US Fiscal Year'])
                                     , us_fiscal_week_number=int(row['US Fiscal Week Number'])
                                     , us_fiscal_year_month_number=int(row['US Fiscal Year Month Number'])
                                     , net_amount=float(row['Net Amount'])
                                     , net_quantity=float(row['Net Quantity'])
                                     )

    return record

def validate_df(df):
    df = df[pd.isnull(df['NARC ID']) == False]
    df = df[df['NARC ID'] != 0]
    df = df[pd.isnull(df['Product Line']) == False]
    df = df[pd.isnull(df['US Fiscal Year']) == False]
    df = df[pd.isnull(df['US Fiscal Week Number']) == False]
    df = df[pd.isnull(df['US Fiscal Year Month Number']) == False]
    return df

def validate_record(record):
    try:
        assert not pd.isnull(record['NARC ID'])
        assert record['NARC ID'] != 0
        assert not pd.isnull(record['Product Line'])
        assert not pd.isnull(record['US Fiscal Year'])
        assert not pd.isnull(record['US Fiscal Week Number'])
        assert not pd.isnull(record['US Fiscal Year Month Number'])
        return record
    except AssertionError as ae:
        logger.debug(ae)
    except Exception as e:
        logger.error(e)

def iterate_directory(dir_path):
    load_queue = Queue(maxsize=1000)
    pool_size = 10
    worker_pool = ThreadPool(pool_size)
    for i in range(pool_size):
        worker_pool.apply_async(load_worker, args=(load_queue,))
    files = os.listdir(dir_path)
    for file in files:
        filename, file_extension = os.path.splitext(file)
        if file_extension == '.csv':
            fp = os.path.join(dir_path, file)
            logger.info('Loading file: %s' %(fp))
            load_from_csv(fp, load_queue)
    for i in range(pool_size):
        load_queue.put('poison')
    worker_pool.close()
    worker_pool.join()

if __name__=='__main__':
    # iterate_directory(r'K:\Data Management\Share\Analytics\Zoetis\Ecosystem_Files\Testing\Zoetis Products by Week')
    iterate_directory(r'C:\Users\mdassow\Documents\Testing\Zoetis_Ecosystem_POC\Ecosystem_Files\Testing\Zoetis Products by Week')