from sqlalchemy import create_engine, Column, Integer, String, Float, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
import datetime
import pandas as pd
import logging
import os
from tqdm import tqdm
from multiprocessing import Queue
from multiprocessing.pool import ThreadPool
tqdm.pandas(desc="my bar!")
logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from sql_server import sql_server_conn

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)
# conn = engine.connect()\
#     .execution_options(schema_translate_map=
#                               {None: "zoetis"}
#                        )
Base = declarative_base()

class zoetis_products_by_week(Base):
    __tablename__ = 'products_by_week'
    __table_args__ = {"schema": "zoetis"}
    narc_id = Column(Integer, primary_key=True)
    narc_postal_code = Column(String)
    narc_state = Column(String)
    product_line = Column(String(100), primary_key=True)
    therapeutic_group = Column(String)
    product_species_group_description = Column(String)
    us_fiscal_year = Column(Integer, primary_key=True)
    us_fiscal_week_number = Column(Integer, primary_key=True)
    us_fiscal_year_month_number = Column(Integer, primary_key=True)
    net_amount = Column(Float)
    net_quantity = Column(Float)
    create_date = Column(DateTime, default=datetime.datetime.now())
    update_date = Column(DateTime, default=datetime.datetime.now(), onupdate=datetime.datetime.now())

    def __init__(self, *args, **kwargs):
        __slots__ = 'narc_id', 'narc_postal_code', 'narc_state', 'product_line', 'therapeutic_group', 'product_species_group_description'\
            , 'us_fiscal_year', 'us_fiscal_week_number', 'net_amount', 'net_quantity', 'create_date', 'update_date'
        super().__init__(*args, **kwargs)
        self.narc_id = kwargs['narc_id'] if 'narc_id' in kwargs else None
        self.narc_postal_code = self.handle_null_to_str(kwargs['narc_postal_code']) if 'narc_postal_code' in kwargs else None
        self.narc_state = self.handle_null_to_str(kwargs['narc_state']) if 'narc_state' in kwargs else None
        self.product_line = self.handle_null_to_str(kwargs['product_line']) if 'product_line' in kwargs else None
        self.therapeutic_group = self.handle_null_to_str(kwargs['therapeutic_group']) if 'therapeutic_group' in kwargs else None
        self.product_species_group_description = self.handle_null_to_str(kwargs['product_species_group_description']) if 'product_species_group_description' in kwargs else None
        self.us_fiscal_year = kwargs['us_fiscal_year'] if 'us_fiscal_year' in kwargs else None
        self.us_fiscal_week_number = kwargs['us_fiscal_week_number'] if 'us_fiscal_week_number' in kwargs else None
        self.us_fiscal_year_month_number = kwargs['us_fiscal_year_month_number'] if 'us_fiscal_year_month_number' in kwargs else None
        self.net_amount = kwargs['net_amount'] if 'net_amount' in kwargs else None
        self.net_quantity = kwargs['net_quantity'] if 'net_quantity' in kwargs else None
        self.create_date = datetime.datetime.now()
        self.update_date = datetime.datetime.now()

    def handle_null_to_str(self, val):
        if pd.isnull(val):
            return None
        return val

    def update_fields(self, *args, **kwargs):
        self.narc_postal_code = kwargs['narc_postal_code'] if 'narc_postal_code' in kwargs else None
        self.narc_state = kwargs['narc_state'] if 'narc_state' in kwargs else None
        self.therapeutic_group = kwargs['therapeutic_group'] if 'therapeutic_group' in kwargs else None
        self.product_species_group_description = kwargs['product_species_group_description'] if 'product_species_group_description' in kwargs else None
        self.net_amount = kwargs['net_amount'] if 'net_amount' in kwargs else None
        self.net_quantity = kwargs['net_quantity'] if 'net_quantity' in kwargs else None
        self.update_date = datetime.datetime.now()


conn_str = sql_server_conn.get_conn_string()
engine = create_engine(conn_str, echo=True)
Base.metadata.create_all(engine)

def load_worker(load_queue):
    try:
        conn_str = sql_server_conn.get_conn_string()
        engine = create_engine(conn_str, echo=False)
        session = Session(bind=engine)
        chunk = load_queue.get()
        while True:
            try:
                if type(chunk) == pd.Series:
                    try:
                        session.add_all(chunk)
                        session.flush()
                        session.commit()
                    except IntegrityError as ie:
                        session.rollback()
                        if ie.orig.args[0] == '23000':
                            logger.info(ie)
                            iterate_handle_upserts(session=session, chunk=chunk)
                    except Exception as e:
                        session.rollback()
                        logger.error(e)
                    chunk = load_queue.get()
                elif chunk == 'poison':
                    break
                else:
                    raise Exception('Load worker does not recognize chunk %s' %(chunk))
            except Exception as e:
                logger.error(e)

    except Exception as e:
        logger.error(e)

def iterate_handle_upserts(session: Session, chunk: pd.Series):
    for c in chunk:
        try:
            session.add(c)
            session.commit()
        except IntegrityError as ie:
            session.rollback()
            if ie.orig.args[0] == '23000':
                handle_upserts(session=session, obj=c)
                session.commit()
        except Exception as e:
            session.rollback()
            logger.error(e)

def handle_upserts(session: Session, obj: zoetis_products_by_week):
    try:
        q = session.query(obj.__class__)
        q = q.filter(obj.__class__.narc_id == obj.narc_id
                     , obj.__class__.product_line == obj.product_line
                     , obj.__class__.us_fiscal_year == obj.us_fiscal_year
                     , obj.__class__.us_fiscal_week_number == obj.us_fiscal_week_number
                     , obj.__class__.us_fiscal_year_month_number == obj.us_fiscal_year_month_number)
        record = q.one()
        if record.__dict__ != obj.__dict__:
            record.update_fields(**obj.__dict__)
            session.flush()
    except Exception as e:
        logger.error(e)

def prep_chunk(temp_df, load_queue):
    chunk = temp_df.apply(map_to_table, axis=1)
    load_queue.put(chunk)
    logger.debug('load_queue.qsize %i' % load_queue.qsize())

def load_from_csv(fp, load_queue):
    df = pd.read_csv(fp)
    df = validate_df(df=df)
    truncate_overlapping_dates(df=df)
    try:
        prep_pool_size = 5
        prep_worker_pool = ThreadPool(prep_pool_size)
        offset = 0
        limit = 100
        while len(df) > offset:
            temp_df = df[offset:offset + limit]
            prep_worker_pool.apply_async(prep_chunk, args=(temp_df, load_queue))
            offset += limit
        prep_worker_pool.close()
        prep_worker_pool.join()

    except Exception as e:
        logger.error(e)

def map_to_table(row):
    # row = validate_record(record=row)
    if row is None:
        return
    record = zoetis_products_by_week(narc_id=int(row['NARC ID'])
                                     , narc_postal_code=row['NARC Postal Code']
                                     , narc_state=row['NARC State']
                                     , product_line=row['Product Line']
                                     , therapeutic_group=row['Therapeutic Group']
                                     , product_species_group_description=row['Product Species Group Description']
                                     , us_fiscal_year=int(row['US Fiscal Year'])
                                     , us_fiscal_week_number=int(row['US Fiscal Week Number'])
                                     , us_fiscal_year_month_number=int(row['US Fiscal Year Month Number'])
                                     , net_amount=float(row['Net Amount'])
                                     , net_quantity=float(row['Net Quantity'])
                                     )

    return record

def validate_df(df):
    df = df[pd.isnull(df['NARC ID']) == False]
    df = df[df['NARC ID'] != 0]
    df = df[pd.isnull(df['Product Line']) == False]
    df = df[pd.isnull(df['US Fiscal Year']) == False]
    df = df[pd.isnull(df['US Fiscal Week Number']) == False]
    df = df[pd.isnull(df['US Fiscal Year Month Number']) == False]
    return df

def truncate_overlapping_dates(df: pd.DataFrame):
    """SQL Server does not have a great way to  handle upserts without having to write stored procs on each table.
    This function is to remove data that would be duplicate keys before loading so that
    duplicates don't cause errors in bulk upserting that would then need to be handled individually"""
    conn_str = sql_server_conn.get_conn_string()
    engine = create_engine(conn_str, echo=False)
    session = Session(bind=engine)
    try:
        unique_dates = get_distinct_dates(df=df)
        for k, i in unique_dates.iterrows():
            q = session.query(zoetis_products_by_week).filter(zoetis_products_by_week.us_fiscal_year == str(i['US Fiscal Year'])
                        , zoetis_products_by_week.us_fiscal_week_number == str(i['US Fiscal Week Number']))
            r = q.delete()
        session.commit()
        session.close()
    except Exception as e:
        logger.error(e)

def get_distinct_dates(df: pd.DataFrame):
    unique_dates = df[['US Fiscal Year','US Fiscal Week Number']].drop_duplicates()
    return unique_dates

def validate_record(record):
    try:
        assert not pd.isnull(record['NARC ID'])
        assert record['NARC ID'] != 0
        assert not pd.isnull(record['Product Line'])
        assert not pd.isnull(record['US Fiscal Year'])
        assert not pd.isnull(record['US Fiscal Week Number'])
        assert not pd.isnull(record['US Fiscal Year Month Number'])
        return record
    except AssertionError as ae:
        logger.debug(ae)
    except Exception as e:
        logger.error(e)

def iterate_directory(dir_path):
    load_queue = Queue(maxsize=100)
    pool_size = 15
    worker_pool = ThreadPool(pool_size)
    for i in range(pool_size):
        worker_pool.apply_async(load_worker, args=(load_queue,))
    files = os.listdir(dir_path)
    for file in files:
        filename, file_extension = os.path.splitext(file)
        if file_extension == '.csv':
            fp = os.path.join(dir_path, file)
            logger.info('Loading file: %s' %(fp))
            load_from_csv(fp, load_queue)
    for i in range(pool_size):
        load_queue.put('poison')
    worker_pool.close()
    worker_pool.join()

if __name__=='__main__':
    # s = """NARC ID	NARC Postal Code	NARC State	Product Line	Therapeutic Group	Product Species Group Description	US Fiscal Year	US Fiscal Week Number	US Fiscal Year Month Number	Net Amount	Net Quantity""".split('\t')
    # for i in s:
    #     print(', ' + i.replace(' ','_').lower() + "=v['" + i + "']")

    # load_from_csv(r'K:\Data Management\Share\Analytics\Zoetis\Ecosystem_Files\Testing\Zoetis Products by Week\Adj_Ship_To_Product__TEST.csv')
    # iterate_directory(r'K:\Data Management\Share\Analytics\Zoetis\Ecosystem_Files\Testing\Zoetis Products by Week')
    iterate_directory(r'C:\Users\mdassow\Documents\Testing\Zoetis_Ecosystem_POC\Ecosystem_Files\Testing\Zoetis Products by Week')