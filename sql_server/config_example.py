config = {
    'prod': {
                'env': 'prod'
                , 'sql_server_user': '<USERNAME>'
                , 'sql_server_pw' : '<PASSWORD>'
                , 'sql_server_host' : '<URLORHOSTNAME>'
                , 'database': '<DATABASENAME>'
                , 'log_email_config': {
                                    'email_host' : '<SMTPHOST>'
                                    , 'email_host_port' : '<SMTPPORT>'
                                    , 'logging_email_from_address' : '<FROMADDRESS>'
                                    ,'logging_email_to_address_list' : ['<TOEMAILLIST>']
                                    , 'email_subject': 'Zoetis Data Ecosystem Log'
                                    }
            }
    ,'local': {
                'env': 'local'
                , 'sql_server_user': '<USERNAME>'
                , 'sql_server_pw' : '<PASSWORD>'
                , 'sql_server_host' : '<URLORHOSTNAME>'
                , 'database': '<DATABASENAME>'
                , 'log_email_config': {
                                    'email_host' : '<SMTPHOST>'
                                    , 'email_host_port' : '<SMTPPORT>'
                                    , 'logging_email_from_address' : '<FROMADDRESS>'
                                    ,'logging_email_to_address_list' : ['<TOEMAILLIST>']
                                    , 'email_subject': 'Zoetis Data Ecosystem Log'
                                    }
            }
}
