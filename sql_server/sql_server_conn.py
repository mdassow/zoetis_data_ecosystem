import os
from .config import config
import urllib

def get_conn_string():
    user = config[os.environ.get('python_env')]['sql_server_user']
    pw = config[os.environ.get('python_env')]['sql_server_pw']
    server = config[os.environ.get('python_env')]['sql_server_host']
    database = config[os.environ.get('python_env')]['database']
    params =  urllib.parse.quote_plus('DRIVER={ODBC Driver 17 for SQL Server};SERVER=%s;DATABASE=%s;UID=%s;PWD={%s};' % (server, database, user, pw))
    conn_str = "mssql+pyodbc:///?odbc_connect=%s" % params
    return conn_str